package com.b208.s1;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;

@SpringBootApplication
@RestController
//It will tell springboot that this application will function as an endpoint handling web requests.
public class S1Application {
	//Annotations in Java Springboot marks classes for their intended functionalities.
	//Springbooot upon startup scans for classes and assign behaviors based on their annotation.
	public static void main(String[] args) {
		SpringApplication.run(S1Application.class, args);
	}

	//@GetMapping will map a route or an endpoint to access or run our method.
	//When http://localhost:8080/hello is accessed from a client, we will be able to run the hi() method.
	@GetMapping("/hello")
	public String hi(){
		return "Hi from our Java Springboot App!";
	}

	@GetMapping("/hi")
	public String hi(@RequestParam(value="name", defaultValue="John") String nameParameter){
		//Pass data through the url using our string query
		//http://localhost:8080/hi?name=Joe
		//We retrieve the value of the filed "name" from our URL
		//defaultValue is the fallback value when the query string or request param is empty.
		System.out.println(nameParameter);
		return "Hi! My name is Listher.";
	}

	@GetMapping("/favoriteFood")
	//http://localhost:8080/favoriteFood?food=laing
	public String myFavoriteFood(@RequestParam(value="food", defaultValue="rice") String foodParameter){
		return "Hi! My favorite food is " + foodParameter;
	}

	//Two ways of passing data through the URL by using Java Springboot.
	//Query String using Request Params - directly passing data into the url and getting the data
	//Path Variable - much more similar to Express.js' req.params

	@GetMapping("/greeting/{name}")
	//http://localhost:8080/greeting/Listher
	public String greeting(@PathVariable("name") String nameParams){
		//@PathVariable allows us to extract data directly from the url.
		return "Hello " + nameParams;
	}

	/*Activity*/

	ArrayList<String> enrollees = new ArrayList<>();

	@GetMapping("/enroll")
	//http://localhost:8080/enroll?user=Jeff
	public String enroll(@RequestParam(value="user") String userParams){
		enrollees.add(userParams);
		return "Welcome, " + userParams + "!";
	}

	@GetMapping("/getEnrollees")
	//http://localhost:8080/getEnrollees
	public ArrayList<String> getEnrollees() {
		return enrollees;
	}

	@GetMapping("/courses/{id}")
	//http://localhost:8080/courses/java101
	//http://localhost:8080/courses/sql101
	//http://localhost:8080/courses/jsoop101
	//http://localhost:8080/courses/express101
	public String getCourses(@PathVariable("id") String courseParams){
		switch (courseParams){
			case "java101":
				return "Name: Java 101, Schedule: MWF 8:00 AM-12:00 PM, Price: PHP 3000";
			case "sql101":
				return "Name: SQL 101, Schedule: MWF 1:00 PM-5:00 PM, Price: PHP 3000";
			case "jsoop101":
				return "Name: JS OOP 101, Schedule: MWF 5:30 AM-9:30 PM, Price: PHP 2500";
			default:
				return "Course cannot be found";
		}
	}
}
